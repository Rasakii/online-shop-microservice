# Online shop application deployed to aws cluster

## Prerequisites

Create an iam role for the eks cluster to manage resources on your behalf

![IAM ROLE FOR EKS](img/cluster-role.png)

Create an Amazon Elastic Kubernetes Service cluster and attach the iam role created to it

![EKS CLUSTER](img/cluster.png)

Cluster created

![EKS CLUSTER-CREATED](img/cluster-created.png)

Create an iam role for node groups

![IAM ROLE FOR EKS](img/node-role.png)

Create the worker nodes inside the cluster specifying the desired amount of nodes and attach iam-role for the nodes

![EKS NODE-GROUP](img/node-1.png)

![EKS NODE-GROUP](img/node-2.png)

Nodes Created

![EKS NODE-GROUP](img/nodes.png)

### Step 1 - Command to update your local Kubernetes configuration (kubeconfig) with credentials and cluster info for interacting with an Amazon EKS cluster created prior my-eks-cluster

![KUBE-CONFIG](img/config.png)

2. To get worker nodes in aws cluster

![WORKER NODES](img/node-on-1.png)

3. Deploy pods with services

![DEPLOY NODES](img/deploy-pod.png)

4. An automatic Load Balancer is provisioned, enabling external access to the cluster via a single DNS endpoint.

![Load Balancer](img/load-balancer.png)

5. Access your app with the dns created by the load balancer

![App](img/app.png)

6. Components running on the nodes

![components](img/component.png)
